var limitevideos = 1;
var arreglo_imagenes = [];
var contador = 0;


// Search for a specified string.
function searchVideo(consulta) {

  $("#msg-usuario").attr('disabled','disabled');
  $("#num-videos").attr('disabled','disabled');

  limitevideos = document.getElementById('num-videos').value;

  console.log(limitevideos);
  var request = gapi.client.youtube.search.list(consulta);
  
  request.execute(function(response) {
    var str = JSON.stringify(response.result);
    $.each(response.items, function() {
      if(contador < limitevideos)
      {
        contador++;
        cadena_ids = cadena_ids + this.id.videoId + ',';

        arreglo_imagenes.push(this.snippet.thumbnails.high.url);

        if(contador % 50 == 0)
        {
          cadena_ids = cadena_ids.substring(0, cadena_ids.length-1);
          arreglo_ids.push(cadena_ids);
          cadena_ids = '';
        }
      }
    });

    if(contador < limitevideos)
    {
      var json_video = {
        q: consulta['q'],
        part: "id,snippet",
        pageToken: response.nextPageToken
      };
      searchVideo(json_video);
    }
  
    if(contador >= limitevideos)
    {
      var num_videos_arr = arreglo_imagenes.length;
      var numero_videos = num_videos_arr;
      var contador_imagenes = 0;

      var pagina_actual = 0;
      var numero_por_pagina = 1;

      var numero_filas = 1;
      var numero_columnas = 1;

      var $tabla = $('#my-table');
      var $cuerpo = $tabla.find('tbody').get();

      var estilo = '';

      $('#my-table > tbody').empty();

      $('.paginador').remove();


      if(numero_videos % 2 !== 0)
          numero_videos++;

      if(numero_videos > 10){
          numero_columnas = 5;
          numero_por_pagina = 2;

          numero_filas = numero_videos / numero_columnas

      }
      else
      {
          if(num_videos_arr > 5){
            numero_columnas = numero_videos / 2
            numero_por_pagina = 2;
            numero_filas = 2;
          }
          else{
            numero_columnas = num_videos_arr;
          }
      }

      for(var i=0; i<numero_filas; i++)
      {
          var fila = $('<tr></tr>');

          for(var j=0; j<numero_columnas; j++)
          {
            var cad;
            if(numero_filas > 1)
              cad = "<td> <img class='imgvideos' style='height: 150px' src=' " + arreglo_imagenes[contador_imagenes]  + "' ></td>"
            else
              cad = "<td> <img class='imgvideos'  style='height: 300px' src=' " + arreglo_imagenes[contador_imagenes]  + "' ></td>"

              if(contador_imagenes < num_videos_arr){
                  $(cad).appendTo(fila);
                  contador_imagenes++;
              }
          }

          fila.appendTo($cuerpo)
      }


      var repaginar = function(){
          $tabla.find('tbody tr').hide()
          .slice(pagina_actual * numero_por_pagina, (pagina_actual + 1) * numero_por_pagina).show();
      }

      repaginar();

      var numero_filas = $tabla.find('tbody tr').length;
      var numero_paginas = Math.ceil(numero_filas / numero_por_pagina);

      var $paginador = $('<div class ="paginador"></div>');

      for(var paginas=0; paginas<numero_paginas; paginas++){
          
          $ ('<button class="btn" ></button>').text(paginas + 1 )
              .bind('click', {nuevaPagina: paginas}, function(event) {
                  pagina_actual = event.data ['nuevaPagina'] ;
                  repaginar();
          }).appendTo($paginador).addClass('clickable');

      }

      $paginador.insertBefore($tabla);

      arreglo_imagenes = [];

      $("#msg-usuario").removeAttr('disabled');
      $("#num-videos").removeAttr('disabled');

      document.getElementById('cont1').scrollTop = document.getElementById('cont1').scrollHeight;


      cadena_ids = cadena_ids.substring(0, cadena_ids.length-1);
      arreglo_ids.push(cadena_ids);
      cadena_ids = '';

      for(var i=0; i<arreglo_ids.length; i++){
        var videoIDRequest = gapi.client.youtube.videos.list({
        id: arreglo_ids[i],
        part: 'id,snippet,recordingDetails'
        });

        videoIDRequest.execute(function(response) {

          if (this.recordingDetails && this.recordingDetails.location) {
            console.log(this);
          }
        });
      }
    }
  }); 
}
